import TextEditor from './components/TextEditor/TextEditor';
import {
  BrowserRouter as Router,
  Route,
  Redirect
} from "react-router-dom";
import { v4 as uuidv4 } from "uuid";

const App = () => {
  return (
    <Router>
      <Route path="/" exact>
        <Redirect to={`/documents/${uuidv4()}`}></Redirect>
      </Route>
      <Route path="/documents/:id">
        <TextEditor></TextEditor>
      </Route>
    </Router>
  );
}

export default App;
