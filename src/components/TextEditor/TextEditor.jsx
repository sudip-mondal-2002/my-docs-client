import { useCallback, useEffect, useState } from 'react'
import Quill from 'quill'
import "quill/dist/quill.snow.css"
import "./styles.css"
import { io } from 'socket.io-client'
import { useParams } from 'react-router-dom'
const TOOLBAR = [
    [{ header: [1, 2, 3, 4, 5, 6, false] }],
    [{ font: [] }],
    [{ list: "ordered" }, { list: "bullet" }],
    ["bold", "italic", "underline"],
    [{ color: [] }, { background: [] }],
    [{ script: "sub" }, { script: "super" }],
    [{ align: [] }],
    ["image", "blockquote", "code-block"],
    ["clean"],
]
// const SOCKET_LINK = "http://localhost:8000"

const SOCKET_LINK = "https://my-google-docs-backend.herokuapp.com/"

const TextEditor = () => {
    const { id: documentId } = useParams();
    const [socket, setSocket] = useState();
    const [quil, setQuil] = useState();

    useEffect(() => {
        if (socket == null || quil == null) return;
        socket.once("load-document", document => {
            quil.setContents(document);
            quil.enable();
        });
        socket.emit("get-document", documentId);
    }, [socket, quil, documentId])

    useEffect(() => {
        const s = io(SOCKET_LINK);
        setSocket(s)
        return () => {
            s.disconnect();
        }
    }, [])

    useEffect(() => {
        if (socket == null || quil == null) return;
        const changeHandler = (delta, oldDelta, source) => {
            if (source !== 'user') return;
            socket.emit("send-changes", delta, quil.getContents());
        }
        quil.on('text-change', changeHandler);
        return () => {
            quil.off('text-change', changeHandler);
        }
    }, [socket, quil])

    useEffect(() => {
        if (socket == null || quil == null) return;
        const changeHandler = delta => {
            quil.updateContents(delta)
        }
        socket.on('recieve-changes', changeHandler);
        return () => {
            socket.off('recieve-changes', changeHandler);
        }
    }, [socket, quil])

    const wrapperRef = useCallback((wrapper) => {
        if (wrapper == null) return;
        wrapper.innerHTML = ''
        const editor = document.createElement("div")
        wrapper.append(editor)
        const q = new Quill(editor, {
            theme: 'snow', modules: {
                toolbar: TOOLBAR
            }
        })
        q.disable();
        q.setText("");
        setQuil(q)
    }, [])

    return (<div className="editor" ref={wrapperRef}></div>)
}

export default TextEditor;